# Internal
import typing as T
import unittest


class TestImport(unittest.TestCase):
    def test_import(self) -> None:
        # External
        from antenna.network import (
            SSLConfig_t,
            SerializationContext,
            __version__,
            create_antenna_server,
            create_antenna_connection,
        )

        self.assertIsInstance(__version__, str)
        self.assertIsNotNone(T.get_origin(SSLConfig_t))
        self.assertIsInstance(SerializationContext, type)
        self.assertTrue(callable(create_antenna_server))
        self.assertTrue(callable(create_antenna_connection))


if __name__ == "__main__":
    unittest.main()
