# Internal
import asyncio
import unittest
from asyncio import wait_for
from unittest.mock import Mock, call

# External
from antenna.network import create_antenna_server, create_antenna_connection


class TestBasicConnection(unittest.IsolatedAsyncioTestCase):
    async def test_connection(self):
        async def connect():
            await asyncio.sleep(0.1)
            channel = await create_antenna_connection(host="127.0.0.1")
            await asyncio.sleep(0.1)
            channel.close()
            await channel.drain()

        async def serve():
            async for channel in create_antenna_server(host="127.0.0.1"):
                await asyncio.sleep(0.1)
                channel.close()
                await channel.drain()
                break

        await asyncio.gather(serve(), connect())

    async def test_connection_client_send_data(self):
        mock = Mock()

        async def connect():
            await asyncio.sleep(0.1)
            channel = await create_antenna_connection(host="127.0.0.1")
            await asyncio.gather(
                channel.write((1, 2)),
                channel.write({"a": 10, "b": 10}),
                channel.write(None),
                channel.write([None, 1, b"12315", {}, [{"a": None}], (1, 2)]),
            )
            await asyncio.sleep(0.1)
            channel.close()
            await channel.drain()

        async def serve():
            async for channel in create_antenna_server(host="127.0.0.1"):
                async for pkg in channel:
                    mock(pkg)
                break

        await wait_for(asyncio.gather(serve(), connect()), 1)

        mock.assert_has_calls(
            [
                call((1, 2)),
                call({"a": 10, "b": 10}),
                call(None),
                call([None, 1, b"12315", {}, [{"a": None}], (1, 2)]),
            ],
            any_order=True,
        )


if __name__ == "__main__":
    unittest.main()
