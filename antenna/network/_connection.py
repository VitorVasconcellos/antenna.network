from __future__ import annotations

# Internal
import typing as T
from ssl import Purpose as SSLPurpose
from socket import AF_INET
from asyncio import get_running_loop

# Project
from ._channel import Channel
from ._protocol import Protocol
from ._security.normalize import SSLConfig_t, normalize_secure_context

# Type generics
K = T.TypeVar("K")


async def create_antenna_connection(
    host: str,
    port: int = 58008,
    *,
    ssl: SSLConfig_t = None,
) -> Channel[K]:

    protocol: Protocol[K] = Protocol(SSLPurpose.CLIENT_AUTH)
    await get_running_loop().create_connection(
        lambda: protocol,
        host,
        port,
        ssl=normalize_secure_context(SSLPurpose.SERVER_AUTH, ssl),
        # TODO: Implement support for other connection family types
        family=AF_INET,
    )

    return await protocol
