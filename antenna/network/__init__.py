"""Antenna Protocol.

Logic for data serialization and socket handling.
"""

# Internal
from importlib.metadata import version

# Project
from ._server import create_antenna_server
from ._connection import create_antenna_connection
from ._stub.pyarrow import SerializationContext
from ._security.normalize import SSLConfig_t

try:
    __version__: str = version(__name__)
except Exception:  # pragma: no cover
    # Internal
    import traceback
    from warnings import warn

    warn(f"Failed to set version due to:\n{traceback.format_exc()}", ImportWarning)

    __version__ = "0.0.0a0"


__all__ = (
    "__version__",
    "SSLConfig_t",
    "SerializationContext",
    "create_antenna_server",
    "create_antenna_connection",
)
