from __future__ import annotations

# Internal
from typing import List, Optional

# External
from typing_extensions import TypedDict

# Project
from ._pyarrow import Buffer


# Source:
#   https://github.com/apache/arrow/blob/apache-arrow-2.0.0/cpp/src/arrow/python/deserialize.h#L41-L52
class SparseTensorCounts(TypedDict):
    coo: int
    csr: int
    csc: int
    csf: int
    ndim_csf: int


# Source:
#   https://github.com/apache/arrow/blob/apache-arrow-2.0.0/python/pyarrow/serialization.pxi#L309-L322
class Components(TypedDict):
    data: Optional[List[Buffer]]
    num_buffers: int
    num_tensors: int
    num_ndarrays: int
    num_sparse_tensors: SparseTensorCounts
