# Internal
from typing import IO, Any, Type, Union, TypeVar, Callable, Optional

# Project
from ._components import Components

# Type generics
K = TypeVar("K")
L = TypeVar("L")

# https://github.com/apache/arrow/blob/apache-arrow-2.0.0/python/pyarrow/io.pxi#L881-L1071
class Buffer:
    size: int
    is_cpu: bool
    parent: Optional[Buffer]
    address: int
    is_mutable: bool

    def hex(self) -> bytes:
        ...

    def __len__(self) -> int:
        ...

    def slice(self, offset: int = 0, length: Optional[int] = None) -> Buffer:
        ...

    def equals(self, other: Buffer) -> bool:
        ...

    def __eq__(self, other: object) -> bool:
        ...

    def __getitem__(self, key: int) -> int:
        ...

    def to_pybytes(self) -> bytes:
        ...


def allocate_buffer(size: int, memory_pool: Any = None, resizable: bool = False) -> Buffer:
    ...


class ArrowException(Exception):
    ...


class SerializedPyObject:
    """
    Arrow-serialized representation of Python object.
    """

    total_bytes: int

    def write_to(self, sink: IO[bytes]) -> None:
        ...

    def to_buffer(self, nthreads: int) -> Buffer:
        ...

    def deserialize(self, context: SerializationContext) -> Any:
        ...

    def to_components(self, memory_pool: Any = None) -> Components:
        ...

    @staticmethod
    def from_components(components: Components) -> SerializedPyObject:
        ...


Buffer_t = Union[Buffer, bytes, bytearray, memoryview]


class SerializationContext:
    def clone(self) -> SerializationContext:
        ...

    def serialize(self, obj: Any) -> SerializedPyObject:
        ...

    def deserialize(self, what: Buffer_t) -> Any:
        ...

    def serialize_to(self, value: Any, sink: IO[bytes]) -> None:
        ...

    def register_type(
        self,
        type_: Type[K],
        type_id: str,
        custom_serializer: Callable[[K], L],
        custom_deserializer: Callable[[L], K],
    ) -> None:
        ...

    def deserialize_components(self, what: Components) -> Any:
        ...
