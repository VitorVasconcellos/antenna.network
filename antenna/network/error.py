from __future__ import annotations


class AntennaProtocolError(Exception):
    """Antenna protocol error."""


class SerializationError(AntennaProtocolError, ValueError):
    """Package serialization error."""


class EncodeError(SerializationError):
    """Package serialization encoding error."""


class DecodeError(SerializationError):
    """Package serialization decoding error."""


__all__ = ("EncodeError", "DecodeError", "SerializationError", "AntennaProtocolError")
