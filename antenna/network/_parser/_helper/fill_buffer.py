from __future__ import annotations

# Internal
import typing as T


def fill_buffer(buffer: memoryview) -> T.Generator[memoryview, T.Optional[int], None]:
    # https://bugs.python.org/issue34905
    buffer = buffer.cast("B")
    offset = 0
    while offset < len(buffer):
        offset += (yield buffer[offset:]) or 0

    # Offset must be the same value as buffer length
    assert offset == len(buffer)
