from __future__ import annotations

# Internal
import typing as T
from struct import Struct


class SubHeader(T.TypedDict):
    buffer_size: int
    buffer_checksum: int


# https://docs.python.org/3/library/struct.html#format-strings
STRUCTURE = Struct(">QL")


def encode(buffer_size: int, buffer_checksum: int) -> memoryview:
    return memoryview(STRUCTURE.pack(buffer_size, buffer_checksum))


def decode(buffer: memoryview) -> SubHeader:
    (buffer_size, buffer_checksum) = STRUCTURE.unpack_from(buffer)
    return {
        "buffer_size": buffer_size,
        "buffer_checksum": buffer_checksum,
    }
