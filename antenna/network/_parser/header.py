from __future__ import annotations

# Internal
import typing as T
from struct import Struct

# Project
from .._stub.pyarrow import Components


class Header(T.TypedDict):
    components: Components
    payload_size: int
    payload_length: int


# https://docs.python.org/3/library/struct.html#format-strings
STRUCTURE = Struct(">Q8L")


def encode(
    payload_size: int,
    num_buffers: int,
    num_tensors: int,
    num_ndarrays: int,
    coo: int,
    csr: int,
    csc: int,
    csf: int,
    ndim_csf: int,
) -> memoryview:
    return memoryview(
        STRUCTURE.pack(
            payload_size, num_buffers, num_tensors, num_ndarrays, coo, csr, csc, csf, ndim_csf
        )
    )


def decode(buffer: memoryview) -> Header:
    (
        payload_size,
        num_buffers,
        num_tensors,
        num_ndarrays,
        coo,
        csr,
        csc,
        csf,
        ndim_csf,
    ) = STRUCTURE.unpack_from(buffer)

    # sparse_tensor_count calculation
    # Source:
    #   https://github.com/apache/arrow/blob/apache-arrow-2.0.0/cpp/src/arrow/python/deserialize.h#L49-L51
    num_total_buffers = coo * 3 + csr * 4 + csc * 4 + 2 * ndim_csf + csf

    # data_length calculation
    # Source:
    #   https://github.com/apache/arrow/blob/apache-arrow-2.0.0/cpp/src/arrow/python/deserialize.cc#L392-L394
    data_length = 1 + num_tensors * 2 + num_total_buffers + num_ndarrays * 2 + num_buffers

    return {
        "components": {
            "data": None,
            "num_buffers": num_buffers,
            "num_tensors": num_tensors,
            "num_ndarrays": num_ndarrays,
            "num_sparse_tensors": {
                "coo": coo,
                "csr": csr,
                "csc": csc,
                "csf": csf,
                "ndim_csf": ndim_csf,
            },
        },
        "payload_size": payload_size,
        "payload_length": data_length,
    }
