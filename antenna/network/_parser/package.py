from __future__ import annotations

# Internal
import typing as T
from zlib import crc32

# Project
from . import header, sub_header
from ..error import DecodeError, EncodeError
from ._helper import fill_buffer
from .._stub.pyarrow import Buffer, allocate_buffer

# Constants
PAYLOAD_SIZE_LIMIT = 1024 * 1024 * 1024  # 1 Gigabytes

# FIXME: Remove all T.cast(buffer, ...) after typeshed resolves:
#  https://github.com/python/typing/issues/593


def encode(components: header.Components) -> T.Iterable[memoryview]:
    assert components["data"] is not None

    # Make a shallow copy of data, as to not modify the original array reference
    data = components["data"][:]
    payload_size = sum(len(buffer) for buffer in data)

    # Validate size
    if payload_size <= 0:
        raise EncodeError(f"Package size is zero")
    if payload_size > PAYLOAD_SIZE_LIMIT:
        raise EncodeError(
            f"Package size ({payload_size} bytes) is beyond limit ({PAYLOAD_SIZE_LIMIT} bytes)"
        )

    num_buffers = components["num_buffers"]
    num_tensors = components["num_tensors"]
    num_ndarrays = components["num_ndarrays"]
    num_sparse_tensors_coo = components["num_sparse_tensors"]["coo"]
    num_sparse_tensors_csr = components["num_sparse_tensors"]["csr"]
    num_sparse_tensors_csc = components["num_sparse_tensors"]["csc"]
    num_sparse_tensors_csf = components["num_sparse_tensors"]["csf"]
    num_sparse_tensors_ndim_csf = components["num_sparse_tensors"]["ndim_csf"]

    # Don't keep reference for longer than necessary
    del components

    yield header.encode(
        payload_size,
        num_buffers,
        num_tensors,
        num_ndarrays,
        num_sparse_tensors_coo,
        num_sparse_tensors_csr,
        num_sparse_tensors_csc,
        num_sparse_tensors_csf,
        num_sparse_tensors_ndim_csf,
    )

    # Reverse and convert Pyarrow Buffers to memoryview
    memory_views = iter(memoryview(T.cast(bytes, buffer)) for buffer in reversed(data))
    while True:
        try:
            memory_view = next(memory_views)
        except StopIteration:
            break

        with memory_view as buffer:
            yield sub_header.encode(len(buffer), crc32(buffer))
            yield buffer


def decode() -> T.Generator[memoryview, T.Optional[int], header.Components]:
    with memoryview(
        T.cast(bytes, allocate_buffer(header.STRUCTURE.size, resizable=False))
    ) as buffer:
        yield from fill_buffer(buffer)
        package_header = header.decode(buffer)

    components = package_header["components"]
    payload_size = package_header["payload_size"]
    payload_length = package_header["payload_length"]

    # Don't keep reference for longer than necessary
    del package_header

    if payload_size <= 0:
        raise DecodeError(f"Package size is zero")
    if payload_size > PAYLOAD_SIZE_LIMIT:
        raise DecodeError(f"Package size ({payload_size} bytes) is beyond the limit")

    data: T.List[Buffer] = []
    total_size = 0
    with memoryview(
        T.cast(bytes, allocate_buffer(sub_header.STRUCTURE.size, resizable=False))
    ) as sub_buffer:
        for _ in range(payload_length):
            yield from fill_buffer(sub_buffer)
            data_header = sub_header.decode(sub_buffer)

            size = data_header["buffer_size"]
            total_size += size
            if total_size > payload_size:
                raise DecodeError("Message size is different from expected")

            pa_buffer = allocate_buffer(size, resizable=False)
            with memoryview(T.cast(bytes, pa_buffer)) as buffer:
                yield from fill_buffer(buffer)

                if crc32(buffer) != data_header["buffer_checksum"]:
                    raise DecodeError("Message checksum is invalid")

            data.append(pa_buffer)

            # Don't keep reference for longer than necessary
            del pa_buffer

    if total_size != payload_size:
        raise DecodeError("Message size is different from expected")

    components["data"] = data

    return components
