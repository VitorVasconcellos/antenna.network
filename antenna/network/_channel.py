from __future__ import annotations

# Internal
import typing as T
from asyncio import (
    Lock,
    Event,
    Future,
    BaseTransport,
    WriteTransport,
    ensure_future,
    get_running_loop,
)
from collections import deque

# Project
from .error import DecodeError, EncodeError
from ._parser import encode
from ._stub.pyarrow import ArrowException, SerializationContext
from ._parser.header import Components

# Type generics
K = T.TypeVar("K")


def serialize(serialization_context: SerializationContext, package: T.Any) -> Components:
    return serialization_context.serialize(package).to_components()


class Channel(T.ContextManager["Channel[K]"], T.AsyncIterator[K]):
    def __init__(self, transport: BaseTransport, *, writable: bool = True):
        self.transport: T.Final = transport
        self.serialization_context: T.Final = SerializationContext()

        self._closed: bool = False
        self._packages: T.Final[T.Deque[Future[K]]] = deque()
        self._write_lock: T.Final = Lock()
        self._available_read: T.Final = Event()
        self._available_write: T.Final = Event()

        if writable:
            self._available_write.set()
        else:
            self._available_write.clear()

    def __exit__(
        self,
        exc_type: T.Optional[T.Type[BaseException]],
        _: T.Any,
        __: T.Any,
    ) -> T.Literal[False]:
        if exc_type and exc_type not in (SystemExit, KeyboardInterrupt):
            self.close()

        return False

    def __enter__(self) -> Channel[K]:
        if self.closed:
            raise ConnectionError("Transport is closed")

        return self

    def __aiter__(self) -> T.AsyncIterator[K]:
        return self

    async def __anext__(self) -> K:
        while len(self._packages) < 1:
            if self.closed:
                raise StopAsyncIteration

            self._available_write.clear()
            await self._available_write.wait()

        try:
            return await self._packages.popleft()
        except Exception as exc:
            self.close()
            raise DecodeError("Package deserialization failed") from exc

    def close(self) -> None:
        self._closed = True

        if not self.transport.is_closing():
            self.transport.close()

        # Release all write and read actions
        self._available_read.set()
        self._available_write.set()

    @property
    def closed(self) -> bool:
        if self._closed:
            return True
        elif self.transport.is_closing():
            self.close()
            return True
        return False

    def deserialize(self, components: Components) -> None:
        with self:
            self._packages.append(
                ensure_future(
                    get_running_loop().run_in_executor(
                        None, self.serialization_context.deserialize_components, components
                    )
                )
            )
            self._available_read.set()

    def pause_writing(self) -> None:
        if not self.closed:
            self._available_write.clear()

    def resume_writing(self) -> None:
        self._available_write.set()

    async def drain(self) -> T.Sequence[K]:
        return tuple([pkg async for pkg in self])

    async def write(self, package: K) -> None:
        # Validate that this is a writable connection
        if not isinstance(self.transport, WriteTransport):
            raise ConnectionError("Connection transport is not writable")

        with self:
            serialization_task: T.Final = get_running_loop().run_in_executor(
                None, serialize, self.serialization_context, package
            )

            # Don't keep external object references for longer than necessary
            del package

            try:
                components = encode(await serialization_task)
            except ArrowException as error:
                raise EncodeError("Package serialization failed") from error

            # Lock to avoid intertwining multiple package data
            async with self._write_lock:
                for buffer in components:
                    # Wait paused transport to resume
                    await self._available_write.wait()

                    if self.closed:
                        raise ConnectionError("Closed transport: dropping pending write request")

                    self.transport.write(buffer)
