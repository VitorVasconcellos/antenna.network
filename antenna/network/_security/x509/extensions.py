from __future__ import annotations

# Internal
import typing as T
from ssl import Purpose as SSLPurpose

# External
from cryptography import x509
from cryptography.hazmat.backends import default_backend

# Generic types
Certificate_t = T.Union[x509.Certificate, bytes]

EXTENSIONS: T.Dict[
    SSLPurpose, T.Dict[x509.ObjectIdentifier, T.Callable[[x509.ExtensionType], bool]]
] = {
    SSLPurpose.CLIENT_AUTH: {
        x509.ExtensionOID.EXTENDED_KEY_USAGE: lambda extension: (
            isinstance(extension, x509.ExtendedKeyUsage)
            and set(extension) >= {x509.OID_CLIENT_AUTH}
        )
    },
    SSLPurpose.SERVER_AUTH: {
        x509.ExtensionOID.EXTENDED_KEY_USAGE: lambda extension: (
            isinstance(extension, x509.ExtendedKeyUsage)
            and set(extension) >= {x509.OID_SERVER_AUTH}
        )
    },
}


def validate_extensions(_certificate: Certificate_t, purpose: SSLPurpose) -> None:
    certificate = (
        x509.load_der_x509_certificate(_certificate, default_backend())
        if isinstance(_certificate, bytes)
        else _certificate
    )

    if purpose not in EXTENSIONS:
        raise ValueError("Invalid ssl Purpose")

    for oid, validator in EXTENSIONS[purpose].items():
        if not validator(certificate.extensions.get_extension_for_oid(oid).value):
            raise RuntimeError(f"Remote certificate validation for {oid} failed")
