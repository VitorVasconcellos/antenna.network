from __future__ import annotations

# Internal
import typing as T
from ssl import Purpose as SSLPurpose, SSLContext
from pathlib import Path
from tempfile import NamedTemporaryFile
from warnings import warn
from contextlib import ExitStack

# External
from antenna.security import create_client_ssl_context, create_server_ssl_context
from packaging.version import Version

# Project
from .adhoc import generate_adhoc_pair


class AbstractSSLContext(T.TypedDict, total=False):
    ca_file: T.Optional[T.Union[str, Path]]
    key_file: T.Optional[T.Union[str, Path]]
    cert_file: T.Optional[T.Union[str, Path]]
    check_hostname: bool


SSLConfig_t = T.Optional[T.Union[SSLContext, AbstractSSLContext]]


def normalize_secure_context(purpose: SSLPurpose, ctx: SSLConfig_t = None) -> SSLContext:
    # External
    from antenna.network import __version__

    if isinstance(ctx, SSLContext):
        context = ctx
    else:
        ctx = ctx or {}
        ca_file = ctx.get("ca_file", None)
        key_file = ctx.get("key_file", None)
        cert_file = ctx.get("cert_file", None)

        with ExitStack() as stack:
            if __debug__ and cert_file is None and key_file is None:
                warn(
                    "No ssl cert/key pair was provided so we are generating one adhoc",
                    category=RuntimeWarning,
                )

                cert, key = generate_adhoc_pair(purpose)

                cert_temp_file = stack.enter_context(NamedTemporaryFile(mode="w+b"))
                cert_temp_file.write(cert)
                cert_temp_file.flush()
                cert_file = cert_temp_file.name

                key_temp_file = stack.enter_context(NamedTemporaryFile(mode="w+b"))
                key_temp_file.write(key)
                key_temp_file.flush()
                key_file = key_temp_file.name

                ca_file = None
            elif cert_file is None or key_file is None:
                raise ValueError("cert and key file paths must both be provided")

            if purpose is SSLPurpose.CLIENT_AUTH:
                context = create_server_ssl_context(cert_file, key_file, ca_file=ca_file)
            elif purpose is SSLPurpose.SERVER_AUTH:
                context = create_client_ssl_context(
                    cert_file,
                    key_file,
                    ca_file=ca_file,
                    check_hostname=ctx.get("check_hostname", False),
                )
            else:
                raise RuntimeError("Invalid ssl purpose")

    context.set_alpn_protocols([f"antenna/{Version(__version__).major}"])

    return context
