from __future__ import annotations

# Internal
import typing as T
from ssl import Purpose as SSLPurpose
from datetime import datetime as dt, timedelta

# External
from cryptography import x509
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes, serialization
from cryptography.hazmat.primitives.asymmetric import rsa


def generate_adhoc_pair(purpose: SSLPurpose) -> T.Tuple[bytes, bytes]:
    """Generate an adhoc ssl certificate and key.

    This a modified version of an internal function in werkzeug by Pallets.
    Original: https://github.com/pallets/werkzeug/blob/ef545f0/src/werkzeug/serving.py#L424-L462
    Licensed under: BSD-3-Clause (https://github.com/pallets/werkzeug/blob/ef545f0/LICENSE.rst)

    """
    private_key = rsa.generate_private_key(
        public_exponent=65537, key_size=2048, backend=default_backend()
    )

    subject = x509.Name(
        [
            x509.NameAttribute(x509.NameOID.ORGANIZATION_NAME, "Dummy Certificate"),
            # pretty damn sure this is not actually accepted by anyone
            x509.NameAttribute(x509.NameOID.COMMON_NAME, "*"),
        ]
    )

    cert_builder = (
        x509.CertificateBuilder()
        .subject_name(subject)
        .issuer_name(subject)
        .public_key(private_key.public_key())
        .serial_number(x509.random_serial_number())
        .not_valid_before(dt.utcnow())
        .not_valid_after(dt.utcnow() + timedelta(days=30))
    )

    if purpose is SSLPurpose.SERVER_AUTH:
        cert_builder = cert_builder.add_extension(
            x509.ExtendedKeyUsage([x509.OID_SERVER_AUTH]), critical=False
        )
        cert_builder = cert_builder.add_extension(
            x509.SubjectAlternativeName([x509.DNSName("*")]), critical=False
        )
    elif purpose is SSLPurpose.CLIENT_AUTH:
        cert_builder = cert_builder.add_extension(
            x509.ExtendedKeyUsage([x509.OID_CLIENT_AUTH]), critical=False
        )
    else:
        raise ValueError("Invalid ssl Purpose")

    return (
        cert_builder.sign(private_key, hashes.SHA256(), default_backend()).public_bytes(
            serialization.Encoding.PEM
        ),
        private_key.private_bytes(
            format=serialization.PrivateFormat.TraditionalOpenSSL,
            encoding=serialization.Encoding.PEM,
            encryption_algorithm=serialization.NoEncryption(),
        ),
    )
