from __future__ import annotations

# Internal
import typing as T
from ssl import Purpose as SSLPurpose, SSLObject, SSLSocket
from asyncio import Future, BaseTransport, BufferedProtocol, get_running_loop

# Project
from ._parser import decode
from ._channel import Channel
from ._security import x509

# Type generics
K = T.TypeVar("K")


class Protocol(T.Generic[K], BufferedProtocol):
    """Antenna Protocol.

    This is the definition of a simple remote process communication protocol focused in exchanging
    python's object data through a network socket.

    State machine of calls:
      start -> CM [-> GB [-> BU?]]* [-> ER?] -> CL -> end

    * CM: connection_made()
    * GB: get_buffer()
    * BU: buffer_updated()
    * ER: eof_received()
    * CL: connection_lost()

    """

    def __init__(self, ssl_purpose: SSLPurpose) -> None:

        self._fut: T.Optional[Future[Channel[K]]] = None
        self._channel: T.Optional[Channel[K]] = None
        self._decoder: T.Optional[T.Generator[memoryview, int, T.Any]] = None
        self._writable: bool = True
        self._ssl_purpose: T.Final = ssl_purpose

    def __await__(self) -> T.Generator[T.Any, None, Channel[K]]:
        if self._fut is None:
            self._fut = get_running_loop().create_future()
        return self._fut.__await__()

    @property
    def _guard(self) -> T.ContextManager[Channel[K]]:
        if self._channel is None:
            raise ConnectionError("Connection not established yet")

        return self._channel

    def get_buffer(self, size_hint: int) -> bytearray:
        with self._guard:
            if self._decoder is None:
                # Instantiate a new decoder if needed
                self._decoder = decode()

            # Execute a new pass of the decoder.
            # Decoder must not fail here, so no StopIteration guard.
            memory_view = next(self._decoder)
            assert not memory_view.readonly

            # TODO: Open an issue about BufferedProtocol.get_buffer returning memoryview
            return T.cast(bytearray, memory_view)

    def eof_received(self) -> bool:
        # Prevent a warning in SSLProtocol.eof_received:
        # "returning true from eof_received() has no effect when using ssl"
        return False

    def pause_writing(self) -> None:
        self._writable = False
        if channel := self._channel:
            channel.pause_writing()

    def resume_writing(self) -> None:
        self._writable = True
        if channel := self._channel:
            channel.resume_writing()

    def buffer_updated(self, length: int) -> None:
        with self._guard as channel:
            if self._decoder is None:
                raise ValueError(
                    "Decoder is empty, `buffer_updated` was probably called before `get_buffer`"
                )

            try:
                self._decoder.send(length)
            except StopIteration as exc:  # Decoder finished executing
                # Clear used decoder
                self._decoder = None

                # Enqueue package's parsing task in another thread
                channel.deserialize(exc.value)

    def connection_made(self, transport: BaseTransport) -> None:
        if self._channel is None:
            self._channel = Channel(transport)
        else:
            if not transport.is_closing():
                transport.close()
            raise RuntimeError("New connection established on an occupied protocol")

        with self._guard as channel:
            ssl_socket: T.Final = transport.get_extra_info("ssl_object")
            if ssl_socket is None:
                # Unencrypted connection must be reject as early as possible
                raise RuntimeError("All connections must have SSL enabled")
            assert isinstance(ssl_socket, SSLSocket) or isinstance(ssl_socket, SSLObject)

            # Retrieve remote certificate
            certificate_bytes: T.Final = ssl_socket.getpeercert(binary_form=True)
            if certificate_bytes is None:
                if not __debug__:
                    # Don't accept connections without a certificate authority in production
                    raise RuntimeError("All connections must have a valid SSL certificate")
            else:
                # Validate remote certificate extensions
                x509.validate_extensions(certificate_bytes, self._ssl_purpose)

            if self._fut is None:
                self._fut = get_running_loop().create_future()
            self._fut.set_result(channel)

    def connection_lost(self, exc: T.Optional[Exception]) -> None:
        """Reset Transport to be reused

        Args:
            exc: Possible error returned during connection closing.

        """
        self._fut = None

        if channel := self._channel:
            channel.close()
            self._channel = None

        if self._decoder:
            self._decoder.close()
            self._decoder = None
