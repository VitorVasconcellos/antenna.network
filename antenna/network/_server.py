from __future__ import annotations

# Internal
import typing as T
from ssl import Purpose as SSLPurpose
from socket import AF_INET
from asyncio import Event, get_running_loop
from collections import deque

# Project
from ._channel import Channel
from ._protocol import Protocol
from ._security.normalize import SSLConfig_t, normalize_secure_context

# Type generics
K = T.TypeVar("K")


async def create_antenna_server(
    host: T.Union[T.List[str], str] = "0.0.0.0",
    port: int = 58008,
    *,
    ssl: SSLConfig_t = None,
) -> T.AsyncIterable[Channel[K]]:
    protocols: T.Deque[Protocol[K]] = deque()
    new_connection = Event()

    def protocol_factory() -> Protocol[K]:
        protocol: Protocol[K] = Protocol(SSLPurpose.SERVER_AUTH)
        protocols.append(protocol)
        new_connection.set()
        return protocol

    async with await get_running_loop().create_server(
        protocol_factory,
        host,
        port,
        ssl=normalize_secure_context(SSLPurpose.CLIENT_AUTH, ssl),
        family=AF_INET,  # TODO: Implement support for other connection family types
    ) as server:
        while server.is_serving():
            if len(protocols) < 1:
                new_connection.clear()
                await new_connection.wait()
            yield await protocols.popleft()
